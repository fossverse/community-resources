![Creative work banner](Assets/Banners/creative_work_banner.svg)

- [Summary](#summary)

## Summary
Anything creative (e.g. artwork, films, and music) from the open source community that does not fit other categories could go here.

- [RemixIcon](https://github.com/Remix-Design/remixicon)  
Remix Icon is a set of open-source icons. Each icon was designed in "Outlined" and "Filled" styles based on a 24x24 grid. Of course, all the icons are free for both personal and commercial use.
