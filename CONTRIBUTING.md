# Contributing
Thank you for taking the time to contribute to this repo! We really appreciate the time and effort you are putting in.

## How to contribute
### Issue
##### Checking if an issue exists
Before you create an issue, you should use the search option to check if someone didn't already request your application/idea. If the idea was already requested, please use that existing issue to continue discussion. If it was rejected and you feel like it was a mistake, do not open a new issue and use the existing one instead.

##### Creating an issue
This is our preferred way of contributing to this project. If you have an idea for a new addition, change or fix, create a new issue following the guidelines specified below where possible.

When creating an issue, make sure to utilise our issue templates. 

Please flare your issue with what type of issue it is.

##### Types of issue

For Android, Applications, or Gaming contributions, please use our Applications issue template.

For Articles, or Education contributions, please use our Articles issue template.

For Research Papers contributions, please use our Research Papers template.

For Creative Work contributions, please use our Applications or Articles issue template, whichever one is most suitable.

### Merge Request
##### Checking if a Merge Request is in progress
Use the search to verify that a Merge Request isn't already being discussed or whether it was rejected. If you feel like a Merge Request was wrongly rejected or feel like you can make a better one then feel free to create a new one.

##### Creating a new Merge Request
If you want to directly contribute a change to the repository files, make sure to follow our guidelines listed below. They will have to follow our style guidelines and will have to be discussed before merging. Use this if you want fix a spelling mistake, bad grammar, etc.

When creating a merge request, make sure to utilise our merge request templates. 

Please flare your merge request with what type of merge request it is.

### Socials
[![Subreddit subscribers](https://img.shields.io/reddit/subreddit-subscribers/fossverse)](https://www.reddit.com/r/FOSSverse/)
[![Discord](https://img.shields.io/discord/704349127865270322?color=5865f2&label=Join%20our%20Discord%21)](https://discord.com/invite/DvFH3Dy)

If you'd like to discuss this repo or any of our other projects in greater detail, or if you just want to have friendly conversations in general, you can join any of our social communities!
