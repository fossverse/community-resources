![Articles banner](Assets/Banners/articles_banner.svg)

- [Summary](#summary)
- [List of Articles](#list-of-articles)

## Summary

Well-written articles and blogs from the open-source community about a variety of topics. These articles are meant only to inform about new software, guides, tutorials, or explanation of software functionality. What will not be included are opinion pieces, or articles primarily covering social issues of any kind. 

<!--
### [Article Title](https:article.com/article1)  
Desc

### [Article Title](https:article.com/article1)  
Desc
-->

## List of Articles

- ### ["Some people think that the problems plaguing Snap also apply to Flatpak, but this is untrue"](https://theevilskeleton.gitlab.io/2021/10/03/flatpak-vs-snap.html) by TheEvilSkeleton
    Informative article debunking the misconception that Flatpak shares the same problems as Snap.

- ### ["What They Don’t Tell You About Setting Up A WireGuard VPN"](https://dev.to/tangramvision/what-they-don-t-tell-you-about-setting-up-a-wireguard-vpn-1h2g) on dev.to
    Wireguard setup tutorial that focuses on explaining what's going on to a greater extent than most "copy-paste" guides.
