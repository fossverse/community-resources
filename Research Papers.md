![Research papers banner](Assets/Banners/research_banner.svg)

- [Summary](#summary)

## Summary

Collection of freely accessible (legal) research papers for learning about a variety of topics (that are not necessarily related to tech or Linux).
