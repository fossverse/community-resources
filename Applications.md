![Applications banner](Assets/Banners/applications_banner.svg)

- [Summary](#summary)
- [List of apps](#list-of-apps)
- [Browser extensions](#browser-extensions)
- [To-do list](#to-do-list)
- [Utilities](#utilities)

## Summary

Great desktop applications from the open-source community. Tested and actively developed, with promising future.

## List of apps

## Browser extensions

- [SponsorBlock](https://github.com/ajayyy/SponsorBlock)  
Browser extension that can automatically skip sponsored segments, gaps, non-music sections, etc.

## To-do list

- [Planner](https://github.com/alainm23/planner)  
A sleek looking planner (to-do list) for Linux.

## Utilities

- [steamtinkerlaunch](https://github.com/frostworx/steamtinkerlaunch/)  
Linux wrapper tool for use with the Steam client for custom launch options and 3rd party programs.

- [Searx](https://github.com/searx/searx)  
Privacy-respecting, hackable metasearch engine

- [RNNoise](https://gitlab.xiph.org/xiph/rnnoise)  
RNNoise is a noise suppression library based on a recurrent neural network.

- [OpenShot](https://github.com/OpenShot/openshot-qt)  
Cross-platform video editor, medium complexity.

- [Ventoy](https://github.com/ventoy/Ventoy)  
Ventoy is an open source tool to create bootable USB drives.
You don't need to format the disk over and over, you just need to copy the image files to the USB drive and boot it. You can still use the drive for storage as an extra!

- [MonkeyType](https://github.com/monkeytypegame/monkeytype)  
Monkeytype is a minimalistic and customizable typing test.

- [lsd](https://github.com/Peltoche/lsd)  
Rewrite of ls with useful extras like more colors, folder icons, extra formatting, etc.

- [OnlyOffice](https://github.com/ONLYOFFICE/DesktopEditors)  
Free office suite alternative.

- [Krita](https://invent.kde.org/graphics/krita)  
Krita is a free and open source digital painting application.

- [Theme park](https://github.com/GilbN/theme.park)  
Customs css themes for many (often self-hosted) apps
