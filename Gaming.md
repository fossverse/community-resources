![gaming](Assets/Banners/gaming_banner.svg)

- [Summary](#summary)
- [Games](#games)
- [Resources](#resources)
  - [Game engines](#game-engines)
  - [Game launchers](#game-launchers)
  - [Game lists](#game-lists)

## Summary
<img src="https://i.imgur.com/kCqH5JJ.png" height=150/>


## Games

- [Mari0](https://github.com/Stabyourself/mari0)  
Mario meets Portal!
- [Mindustry](https://mindustrygame.github.io/)   
Mindustry is a hybrid tower-defense sandbox factory game. Create elaborate supply chains of conveyor belts to feed ammo into your turrets, produce materials to use for building, and defend your structures from waves of enemies.
- [Minetest](https://github.com/minetest/minetest)  
Minetest is an open source voxel game engine with easy modding and game creation. Has "minecraft-like" game among others.

- [Veloren](https://veloren.net/)  
Multiplayer voxel RPG written in Rust. Very much like cube world, but actively maintained and rapidly improving.
## Resources
### Game engines
- [Minetest](https://github.com/minetest/minetest)  
Minetest is an open source voxel game engine with easy modding and game creation. Has "minecraft-like" game among others.

### Game launchers 
- [Lutris](https://lutris.net/)  
Lutris is a free and open source game manager. It provides one-click installation of many games via installation scripts from the developers and the community. It greatly simplifies running games on WINE for when Steam is not an option. It supports multiple gaming platforms and selection of emulators.
- [Playnite](https://playnite.link/)  
Playnite is an open source video game library manager.
- [GameHub](https://tkashkin.github.io/projects/gamehub/)  
GameHub is a unified library for all your games.
- [Heroic Games Launcher](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher)  
Heroic is an Open Source Game Launcher that supports launching games from the Epic Games Store using Legendary and GOG Games using gogdl.

### Game lists
- [OSGL](https://trilarion.github.io/opensourcegames/)  
Open Source Games List
