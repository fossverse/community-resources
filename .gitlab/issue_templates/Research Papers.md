# New research paper
<!---
To tick a box, add an x inside it, like so:
- [x] I agree that [...]
instead of:
- [ ] I agree that [...]
-->
- [ ] I have read the [Contributing](CONTRIBUTING.md) guidelines
- [ ] I checked if someone already created the same issue
- [ ] This article is peer-reviewed

## Research paper link
<!-- This should be a public, free and legal source -->

## Brief description
<!-- This should provide a rough idea of what the research paper is about -->

## Why should it be added?
<!-- Why is the research paper interesting or informative. -->
