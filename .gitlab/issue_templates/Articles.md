# New article
<!---
To tick a box, add an x inside it, like so:
- [x] I agree that [...]
instead of:
- [ ] I agree that [...]
-->
- [ ] I have read the [Contributing](CONTRIBUTING.md) guidelines
- [ ] I checked if someone already created the same issue
- [ ] This article is **not** an [opinion piece](https://en.wikipedia.org/wiki/Opinion_piece)
- [ ] This article's **primary** focus is not a social issue

## Article link

## Brief description
<!-- This should provide a rough idea of what the article is about -->

## Why should it be added?
<!-- Why is the article interesting or informative. It cannot be offensive and ideally shouldn't cover controversial topics -->
