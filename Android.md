![Android banner](Assets/Banners/android_banner.svg)

- [Summary](#summary)
- [List of apps](#list-of-apps)
  - [Authenticators and password management apps](#authenticators-and-password-management-apps)
  - [Chat and Messaging apps](#chat-and-messaging-apps)
  - [Emulators](#emulators)
  - [Games](#games)
  - [Launchers](#launchers)
  - [News and RSS apps](#news-and-rss-apps)
  - [Productivity apps](#productivity-apps)
  - [Utilities](#utilities)

## Summary

Great Android applications from the open-source community. Tested and actively developed, with promising future.

## List of apps

### Authenticators and password management apps

- [Aegis](https://github.com/beemdevelopment/Aegis)  
Aegis Authenticator is a free, secure and open source 2FA app for Android. Supports HOTP and TOTP.
- [Bitwarden](https://github.com/bitwarden/mobile)  
Android application for the Bitwarden password manager.

### Chat and Messaging apps

- [Element Android](https://github.com/vector-im/element-android)  
A glossy Matrix collaboration client for Android.
- [NekoX](https://github.com/NekoX-Dev/NekoX)  
NekoX is a free and open source third-party Telegram client, based on Telegram-FOSS with extra features.
- [Telegram](https://github.com/DrKLO/Telegram)  
Telegram is a messaging app with a focus on speed and security. It’s superfast, simple and free.

### Emulators

- [Limbo x86 emulator](https://github.com/limboemu/limbo)  
QUEMU-based emulator for Android with support for: x86/x86_64, ARM/ARM64 and PowerPC/PowerPC64 Sparc architectures.

### Games

- [Mindustry](https://anuke.itch.io/mindustry)  
Mindustry is a hybrid tower-defense sandbox factory game. Features Multiplayer and Mod support.
- [UnCiv](https://github.com/yairm210/Unciv)  
Open-source Android/Desktop remake of Civ V. Features Multiplayer and Mod support.

### Launchers

- [Blue Line Console](https://github.com/nhirokinet/bluelineconsole)  
Keyboard based launcher for Android. Similar to Rofi in style, can replace existing launcher or be used with a shortcut.

### News and RSS apps

- [Handy News Reader](https://github.com/yanus171/Handy-News-Reader)  
Fully offline capable RSS reader.

### Productivity apps

- [Goodtime](https://github.com/adrcotfas/Goodtime)  
A minimalist but powerful productivity timer designed to keep you focused and free of distractions.
- [Standard Notes](https://github.com/standardnotes/mobile)  
A simple and private notes app with end-to-end encryption support.
- [Tasks.org](https://github.com/tasks/tasks)  
A decent To-Do list app with synchronization functionality. Fork of Astrid Tasks & To-Do List.

### Utilities

- [Amaze](https://github.com/TeamAmaze/AmazeFileManager)  
Material design file manager for Android.
- [Calculator N+](https://github.com/tranleduy2000/ncalc)  
Powerful calculator and math solver.
- [iFixit App](https://github.com/iFixit/iFixitAndroid)  
Official iFixit Android app for all your repair needs.
- [Imagepipe](https://github.com/pedrocr/imagepipe)  
Image processing pipeline.
- [tldr man-pages](https://github.com/tldr-pages/tldr)  
A collection of community-maintained help pages for command-line tools, that aims to be a simpler, more approachable complement to traditional man pages.
- [Blokada](https://github.com/blokadaorg/blokada)  
Open-source mobile ad blocker.
