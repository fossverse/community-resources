![Education banner](Assets/Banners/education_banner.svg)

- [Summary](#summary)
- [Links](#links)

## Summary

Free learning resources about any topic; structured, well-written and up-to-date with modern knowledge.

## Links

- [Flexbugs](https://github.com/philipwalton/flexbugs)  
Curated list of flexbox issues and cross-browser workarounds for them.

- [fossjobs](https://www.fossjobs.net/)  
A job listing website for paid FOSS-related jobs.